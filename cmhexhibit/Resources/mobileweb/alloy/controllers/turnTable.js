function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "turnTable";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.turnTable = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "turnTable"
    });
    $.__views.turnTable && $.addTopLevelView($.__views.turnTable);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#07B5BE",
        font: {
            fontFamily: "Arial",
            fontSize: 30,
            fontWeight: "bold"
        },
        textAlign: "center",
        top: 0,
        backgroundColor: "#041531",
        text: "Turn Table",
        id: "Heading"
    });
    $.__views.turnTable.add($.__views.Heading);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var imageCollection = [ "http://placehold.it/700x300/556270", "http://placehold.it/700x200/4ECDC4", "http://placehold.it/600x300/C7F464", "http://placehold.it/600x200/FF6B6B", "http://placehold.it/700x300/C44D58" ];
    var scrollGallery = Ti.UI.createScrollableView({
        showPagingControl: false,
        maxZoomScale: 5,
        minZoomScale: 1,
        top: 50,
        height: 300,
        width: 700,
        backgroundColor: "#000"
    });
    var viewCollection = [];
    for (var i = 0; imageCollection.length > i; i++) {
        var view = Ti.UI.createView({});
        var img = Ti.UI.createImageView({
            maxZoomScale: 5,
            defaultImage: "images/4ECDC4.png",
            width: 700,
            height: 300
        });
        img.image = imageCollection[i];
        view.add(img);
        viewCollection.push(view);
    }
    scrollGallery.addEventListener("scroll", function() {
        if (scrollGallery.currentPage < imageCollection.length - 1) {
            var nxt = scrollGallery.currentPage + 1;
            scrollGallery.views[nxt].children[0].image = imageCollection[nxt];
        }
    });
    scrollGallery.views = viewCollection;
    $.turnTable.add(scrollGallery);
    var ComponentLabel1 = Ti.UI.createLabel({
        color: "#474448",
        font: {
            fontFamily: "Arial",
            fontSize: 20,
            fontWeight: "bold"
        },
        text: "Random test label",
        textAlign: "center",
        height: 50,
        top: 80
    });
    $.turnTable.add(ComponentLabel1);
    var ComponentLabel2 = Ti.UI.createLabel({
        color: "#FFFFFF",
        backgroundColor: "#041531",
        font: {
            fontFamily: "Arial",
            fontSize: 20,
            fontWeight: "bold"
        },
        text: "Another random test label",
        textAlign: "center",
        height: 50,
        top: 450
    });
    $.turnTable.add(ComponentLabel2);
    $.turnTable.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;