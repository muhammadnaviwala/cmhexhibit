function Controller() {
    function createRow() {
        var row = Ti.UI.createTableViewRow({
            height: 70,
            backgroundColor: "#ffffff"
        });
        return row;
    }
    function pushRow(row) {
        tableData.push(row);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "cupsAndBalls";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.cupsAndBalls = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "cupsAndBalls"
    });
    $.__views.cupsAndBalls && $.addTopLevelView($.__views.cupsAndBalls);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#07B5BE",
        font: {
            fontFamily: "Arial",
            fontSize: 30,
            fontWeight: "bold"
        },
        textAlign: "center",
        top: 0,
        backgroundColor: "#041531",
        text: "Cups and Balls",
        id: "Heading"
    });
    $.__views.cupsAndBalls.add($.__views.Heading);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 230,
        color: "white",
        font: {
            fontSize: 20,
            fontFamily: "Helvetica Neue"
        },
        textAlign: "center",
        backgroundColor: "#1b4A9C",
        top: 70,
        text: "Description",
        id: "Heading"
    });
    $.__views.cupsAndBalls.add($.__views.Heading);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 230,
        color: "white",
        font: {
            fontSize: 20,
            fontFamily: "Helvetica Neue"
        },
        textAlign: "center",
        backgroundColor: "#173E7F",
        top: 300,
        text: "Image",
        id: "Heading"
    });
    $.__views.cupsAndBalls.add($.__views.Heading);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    var post_id = args.post_id;
    var tableData = [];
    Titanium.UI.createView({
        top: 0,
        backgroundColor: "#018F93",
        height: 70
    });
    var row = createRow();
    var eachComponentLabel = Ti.UI.createLabel({
        color: "#474448",
        font: {
            fontFamily: "Arial",
            fontSize: 20,
            fontWeight: "bold"
        },
        text: post_id,
        textAlign: "center",
        height: 50
    });
    row.add(eachComponentLabel);
    pushRow(row);
    var tableView = Ti.UI.createTableView({
        backgroundColor: "#07B5BE",
        data: tableData,
        top: 73
    });
    $.cupsAndBalls.add(tableView);
    $.cupsAndBalls.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;