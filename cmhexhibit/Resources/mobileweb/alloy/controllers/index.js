function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#07B5BE",
        font: {
            fontFamily: "Arial",
            fontSize: 40,
            fontWeight: "bold"
        },
        textAlign: "center",
        top: 0,
        backgroundColor: "#041531",
        text: "Le Components",
        id: "Heading"
    });
    $.__views.index.add($.__views.Heading);
    $.__views.testLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        font: {
            fontSize: 20,
            fontFamily: "Helvetica Neue"
        },
        textAlign: "center",
        id: "testLabel"
    });
    $.__views.index.add($.__views.testLabel);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var webService = require("component");
    webService.getJsonData(function(webData) {
        if (webData) {
            $.testLabel.text = webData.resultsCount;
            var tableData = [];
            Titanium.UI.createView({
                top: 0,
                backgroundColor: "#018F93",
                height: 70
            });
            for (var i = 0; webData.count_total > i; i++) {
                var row = Ti.UI.createTableViewRow({
                    rowIndex: i,
                    height: 70,
                    backgroundColor: "#ffffff"
                });
                webData.posts[i].title.indexOf("Cup") > -1 ? eachComponentLabelLink = "cupsAndBalls" : webData.posts[i].title.indexOf("Disk") > -1 && (eachComponentLabelLink = "turnTable");
                Ti.API.info(eachComponentLabelLink);
                var eachComponentLabel = Ti.UI.createLabel({
                    color: "#474448",
                    font: {
                        fontFamily: "Arial",
                        fontSize: 20,
                        fontWeight: "bold"
                    },
                    text: webData.posts[i].title,
                    textAlign: "center",
                    height: 50,
                    id: webData.posts[i].id,
                    headlineLink: eachComponentLabelLink
                });
                var args = {
                    post_id: webData.posts[i].id
                };
                eachComponentLabel.addEventListener("click", function(e) {
                    $.index.close();
                    var w = Alloy.createController(e.source.headlineLink, args).getView();
                    w.open();
                });
                row.add(eachComponentLabel);
                tableData.push(row);
            }
            var tableView = Ti.UI.createTableView({
                backgroundColor: "#07B5BE",
                data: tableData,
                top: 73
            });
            $.index.add(tableView);
        }
    });
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;