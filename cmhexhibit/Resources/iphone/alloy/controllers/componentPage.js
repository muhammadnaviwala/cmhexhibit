function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "componentPage";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.componentPage = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "componentPage"
    });
    $.__views.componentPage && $.addTopLevelView($.__views.componentPage);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#07B5BE",
        font: {
            fontFamily: "Arial",
            fontSize: 20,
            fontWeight: "bold"
        },
        textAlign: "center",
        top: 0,
        backgroundColor: "#041531",
        text: "Generic Component Page",
        id: "Heading"
    });
    $.__views.componentPage.add($.__views.Heading);
    exports.destroy = function() {};
    _.extend($, $.__views);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;