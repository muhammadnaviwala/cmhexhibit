function Controller() {
    function createPlainRow() {
        var row = Ti.UI.createTableViewRow({
            height: "200dp",
            backgroundColor: "#ffffff"
        });
        return row;
    }
    function createHeadingRow() {
        var row = Ti.UI.createTableViewRow({
            height: "50dp",
            backgroundColor: eachRowColors[eachRowColorsCounter]
        });
        eachRowColorsCounter++;
        return row;
    }
    function addImagesToPrimarySlider(headingText, imageCollection) {
        var row = createPlainRow();
        var headingRow = createHeadingRow();
        var heading = Ti.UI.createLabel({
            color: "#FFFFFF",
            font: {
                fontFamily: "Arial",
                fontSize: 22,
                fontWeight: "bold"
            },
            text: headingText,
            textAlign: "center"
        });
        headingRow.add(heading);
        tableData.push(headingRow);
        var imageWrappers = [];
        for (var i = 0; imageCollection.length > i; i++) {
            var tempImage = Ti.UI.createImageView({
                image: imageCollection[i],
                defaultImage: "http://placehold.it/700x300",
                width: "100%",
                height: "100%"
            });
            var tempWrapper = Ti.UI.createScrollView({
                maxZoomScale: 4
            });
            tempWrapper.add(tempImage);
            imageWrappers[i] = tempWrapper;
        }
        var scrollableView = Ti.UI.createScrollableView({
            views: imageWrappers,
            showPagingControl: true,
            pagingControlTimeout: 0,
            showPagingControl: true,
            maxZoomScale: 5,
            minZoomScale: 1,
            borderRadius: 4,
            height: "90%",
            width: "90%"
        });
        row.add(scrollableView);
        tableData.push(row);
    }
    function addDescription(headingText, descriptionText) {
        var row = createPlainRow();
        var headingRow = createHeadingRow();
        var heading = Ti.UI.createLabel({
            color: "#FFFFFF",
            font: {
                fontFamily: "Arial",
                fontSize: 22,
                fontWeight: "bold"
            },
            text: headingText,
            textAlign: "center"
        });
        headingRow.add(heading);
        tableData.push(headingRow);
        var description = Ti.UI.createLabel({
            color: "#041531",
            font: {
                fontFamily: "Arial",
                fontSize: 20,
                fontWeight: "normal"
            },
            text: descriptionText,
            textAlign: "center"
        });
        Ti.API.info("Description text = " + descriptionText);
        row.add(description);
        tableData.push(row);
    }
    function addBigQuestions(headingText, listOfQuestions) {
        var row = createPlainRow();
        var headingRow = createHeadingRow();
        var arrayLength = listOfQuestions.length;
        Ti.API.info("listOfQuestions Array Length = " + arrayLength);
        var heading = Ti.UI.createLabel({
            color: "#FFFFFF",
            font: {
                fontFamily: "Arial",
                fontSize: 22,
                fontWeight: "bold"
            },
            text: headingText,
            textAlign: "center"
        });
        headingRow.add(heading);
        tableData.push(headingRow);
        var allViews = [];
        for (var i = 0; arrayLength > i; i++) {
            var question = Ti.UI.createLabel({
                color: "#041531",
                font: {
                    fontFamily: "Arial",
                    fontSize: 20,
                    fontWeight: "normal"
                },
                text: listOfQuestions[i],
                textAlign: "center"
            });
            allViews[i] = Ti.UI.createView({});
            allViews[i].add(question);
        }
        var scrollableView = Ti.UI.createScrollableView({
            views: allViews,
            showPagingControl: true,
            pagingControlTimeout: 0,
            showPagingControl: true,
            maxZoomScale: 5,
            minZoomScale: 1,
            borderRadius: 4,
            height: "90%",
            width: "90%"
        });
        row.add(scrollableView);
        tableData.push(row);
    }
    function addSurvery(headingText) {
        var headingRow = createHeadingRow();
        var heading = Ti.UI.createLabel({
            color: "#FFFFFF",
            font: {
                fontFamily: "Arial",
                fontSize: 22,
                fontWeight: "bold"
            },
            text: headingText,
            textAlign: "center"
        });
        headingRow.add(heading);
        tableData.push(headingRow);
        var webRow = Ti.UI.createTableViewRow({
            backgroundColor: eachRowColors[3]
        });
        var webview = Ti.UI.createWebView({
            url: "https://kyleclark.wufoo.com/forms/mjwi4ow01gffjr/",
            top: -40,
            showScrollbar: true
        });
        webRow.add(webview);
        tableData.push(webRow);
        webview.addEventListener("load", function() {
            if ("https://kyleclark.wufoo.com/forms/mjwi4ow01gffjr/" != webview.url) {
                webview.hide();
                webRow.setHeight(0);
            }
        });
    }
    function addVideo(headingText, videoUrl) {
        var row = createPlainRow();
        var headingRow = createHeadingRow();
        var heading = Ti.UI.createLabel({
            color: "#FFFFFF",
            font: {
                fontFamily: "Arial",
                fontSize: 22,
                fontWeight: "bold"
            },
            text: headingText,
            textAlign: "center"
        });
        headingRow.add(heading);
        tableData.push(headingRow);
        var video = Titanium.Media.createVideoPlayer({
            url: videoUrl,
            fullscreen: false,
            autoplay: false
        });
        row.add(video);
        tableData.push(row);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "turnTable";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.turnTable = Ti.UI.createWindow({
        backgroundColor: "#fff",
        id: "turnTable"
    });
    $.__views.turnTable && $.addTopLevelView($.__views.turnTable);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#07B5BE",
        font: {
            fontFamily: "Arial",
            fontSize: 30,
            fontWeight: "bold"
        },
        textAlign: "center",
        top: 0,
        backgroundColor: "#041531",
        text: "Turn Table",
        id: "Heading"
    });
    $.__views.turnTable.add($.__views.Heading);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var tableData = [];
    var eachRowColors = [ "#107FC9", "#0E4EAD", "#0B108C", "#0C0F66", "#07093D" ];
    var eachRowColorsCounter = 0;
    var imageCollection = [ "http://placehold.it/700x300/556270", "http://placehold.it/700x200/4ECDC4", "http://placehold.it/600x300/C7F464", "http://placehold.it/600x200/FF6B6B", "http://placehold.it/700x300/C44D58" ];
    var listOfBigQuestions = [ "Question 1", "Question 2", "Question 3", "Question 4" ];
    addDescription("Description", "Cup and Balls is a fun component where kinetic energy is generated as you launch the balls.");
    addImagesToPrimarySlider("Fun Carousel", imageCollection);
    addBigQuestions("Big Qs", listOfBigQuestions);
    addVideo("Some cool video", "http://vimeo.com/97348308/download?t=1402078893&v=259758967&s=fc21a6e94adaecbf8bd1a2579bedb7d4");
    addSurvery("Take our awesome survery!");
    var tableView = Ti.UI.createTableView({
        backgroundColor: "#FFFFFF",
        data: tableData,
        top: 70
    });
    $.turnTable.title = "Dolphins are cool";
    $.turnTable.add(tableView);
    $.turnTable.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;