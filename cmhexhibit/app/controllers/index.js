var webService = require('component');
var url = "http://excl.dreamhosters.com/dev/api/get_posts/?post_type=component";

webService.getJsonData(url, function(webData) {
	if (webData) {
		$.testLabel.text = webData.resultsCount;
		var tableData = [];

		var block = Titanium.UI.createView({
			top : 0,
			backgroundColor : '#018F93',
			height : 70,
		});

		for (var i = 0; i < webData.count_total; i++) {
			var row = Ti.UI.createTableViewRow({
				rowIndex : i, // custom property, useful for determining the row during events
				height : 70,
				backgroundColor : '#ffffff'
			});

			if (webData.posts[i].title.indexOf("Cup") > -1) {
				eachComponentLabelLink = "cupsAndBalls";
			} else if (webData.posts[i].title.indexOf("Disk") > -1) {
				eachComponentLabelLink = "turnTable";
			}

			Ti.API.info(eachComponentLabelLink);

			// -----------------------------------------------------------------
			var eachComponentLabel = Ti.UI.createLabel({
				color : '#474448',
				font : {
					fontFamily : 'Arial',
					fontSize : 20,
					fontWeight : 'bold'
				},
				text : webData.posts[i].title,
				textAlign : 'center',
				height : 50,
				id : webData.posts[i].id,
				headlineLink : eachComponentLabelLink,
			});

			var args = {
				post_id : webData.posts[i].id
			};

			eachComponentLabel.addEventListener('click', function(e) {
				// $.index.close();
				var w = Alloy.createController(e.source.headlineLink, args).getView();
				w.open();
			});

			row.add(eachComponentLabel);
			// -----------------------------------------------------------------

			tableData.push(row);
		}

		var tableView = Ti.UI.createTableView({
			backgroundColor : '#07B5BE',
			data : tableData,
			top : 73
		});

		$.index.add(tableView);
	}
});

$.index.open();
