// global variables
var tableData = [];
// var eachRowColors = ['#061f49', '#082960', '#0a3378', '#0c3d90', '#0e47a7', '#1051bf', '#125bd7']; 	// max size?
// var eachRowColors = ['#ECD078', '#D95B43', '#C02942', '#542437', '#53777A']; // thought provoking
var eachRowColors = ['#107FC9', '#0E4EAD', '#0B108C', '#0C0F66', '#07093D'];
// deep sky blues
// var eachRowColors = ['#556270', '#4ECDC4', '#C7F464', '#FF6B6B', '#C44D58']; // emo kid
var eachRowColorsCounter = 0;
var imageCollection = ['http://placehold.it/700x300/556270', 'http://placehold.it/700x200/4ECDC4', 'http://placehold.it/600x300/C7F464', 'http://placehold.it/600x200/FF6B6B', 'http://placehold.it/700x300/C44D58'];
// var imageCollection = ['/images/4ECDC4.png', '/images/556270.png', '/images/C44D58.png', '/images/C7F464.png', '/images/FF6B6B.png'];
var listOfBigQuestions = ['Question 1', 'Question 2', 'Question 3', 'Question 4'];

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function createPlainRow() {
	var row = Ti.UI.createTableViewRow({
		// height: (Ti.Platform.displayCaps.platformHeight / 8),
		height : '200dp',
		backgroundColor : '#ffffff',
	});
	return row;
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function createHeadingRow() {
	var row = Ti.UI.createTableViewRow({
		// height: (Ti.Platform.displayCaps.platformHeight / 8),
		height : '50dp',
		backgroundColor : eachRowColors[eachRowColorsCounter],
	});
	eachRowColorsCounter++;
	return row;
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function addImagesToPrimarySlider(headingText, imageCollection) {
	var row = createPlainRow();
	var headingRow = createHeadingRow();

	var heading = Ti.UI.createLabel({
		color : '#FFFFFF',
		font : {
			fontFamily : 'Arial',
			fontSize : 22,
			fontWeight : 'bold'
		},
		text : headingText,
		textAlign : 'center',
	});
	headingRow.add(heading);
	tableData.push(headingRow);

	var imageWrappers = [];
	for (var i = 0; i < imageCollection.length; i++) {
		var tempImage = Ti.UI.createImageView({
			image : imageCollection[i],
			defaultImage : 'http://placehold.it/700x300',
			width : '100%',
			height : '100%'
		});
		var tempWrapper = Ti.UI.createScrollView({
			maxZoomScale : 4.0,
		});
		tempWrapper.add(tempImage);
		imageWrappers[i] = tempWrapper;
	}

	var scrollableView = Ti.UI.createScrollableView({
		views : imageWrappers,
		showPagingControl : true,
		pagingControlTimeout : 0, // Set to less than or equal to 0 to disable timeout, to keep controls displayed.
		showPagingControl : true,
		maxZoomScale : 5,
		minZoomScale : 1,
		borderRadius : 4,
		height : '90%',
		width : '90%',
	});

	row.add(scrollableView);
	tableData.push(row);
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function addDescription(headingText, descriptionText) {
	var row = createPlainRow();
	var headingRow = createHeadingRow();

	var heading = Ti.UI.createLabel({
		color : '#FFFFFF',
		font : {
			fontFamily : 'Arial',
			fontSize : 22,
			fontWeight : 'bold'
		},
		text : headingText,
		textAlign : 'center',
	});
	headingRow.add(heading);
	tableData.push(headingRow);

	var description = Ti.UI.createLabel({
		color : '#041531',
		font : {
			fontFamily : 'Arial',
			fontSize : 20,
			fontWeight : 'normal'
		},
		text : descriptionText,
		textAlign : 'center',
	});
	Ti.API.info("Description text = " + descriptionText);
	row.add(description);
	tableData.push(row);
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function addBigQuestions(headingText, listOfQuestions) {
	var row = createPlainRow();
	var headingRow = createHeadingRow();
	var arrayLength = listOfQuestions.length;
	Ti.API.info("listOfQuestions Array Length = " + arrayLength);

	var heading = Ti.UI.createLabel({
		color : '#FFFFFF',
		font : {
			fontFamily : 'Arial',
			fontSize : 22,
			fontWeight : 'bold'
		},
		text : headingText,
		textAlign : 'center',
	});
	headingRow.add(heading);
	tableData.push(headingRow);

	var allViews = [];

	for (var i = 0; i < arrayLength; i++) {

		var question = Ti.UI.createLabel({
			color : '#041531',
			font : {
				fontFamily : 'Arial',
				fontSize : 20,
				fontWeight : 'normal'
			},
			text : listOfQuestions[i],
			textAlign : 'center',
		});

		allViews[i] = Ti.UI.createView({});
		allViews[i].add(question);
	}

	var scrollableView = Ti.UI.createScrollableView({
		views : allViews,
		showPagingControl : true,
		pagingControlTimeout : 0, // Set to less than or equal to 0 to disable timeout, to keep controls displayed.
		showPagingControl : true,
		maxZoomScale : 5,
		minZoomScale : 1,
		borderRadius : 4,
		height : '90%',
		width : '90%',
	});

	row.add(scrollableView);
	tableData.push(row);
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function addSurvery(headingText) {
	var headingRow = createHeadingRow();
	var heading = Ti.UI.createLabel({
		color : '#FFFFFF',
		font : {
			fontFamily : 'Arial',
			fontSize : 22,
			fontWeight : 'bold'
		},
		text : headingText,
		textAlign : 'center',
	});
	headingRow.add(heading);
	tableData.push(headingRow);

	var webRow = Ti.UI.createTableViewRow({
		// height : 500',
		backgroundColor : eachRowColors[3]
	});

	var webview = Ti.UI.createWebView({
		url : 'https://kyleclark.wufoo.com/forms/mjwi4ow01gffjr/',
		top : -40,
		showScrollbar : true
	});

	webRow.add(webview);
	tableData.push(webRow);

	webview.addEventListener('load', function(e) {
		if (webview.url != 'https://kyleclark.wufoo.com/forms/mjwi4ow01gffjr/') {
			webview.hide();
			webRow.setHeight(0);
		}
	});
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
function addVideo(headingText, videoUrl) {
	var row = createPlainRow();
	var headingRow = createHeadingRow();
	var heading = Ti.UI.createLabel({
		color : '#FFFFFF',
		font : {
			fontFamily : 'Arial',
			fontSize : 22,
			fontWeight : 'bold'
		},
		text : headingText,
		textAlign : 'center',
	});
	headingRow.add(heading);
	tableData.push(headingRow);
	
	var video = Titanium.Media.createVideoPlayer({
		url : videoUrl,
		fullscreen : false,
		autoplay : false,
	});
	row.add(video);
	tableData.push(row);
}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------

addDescription("Description", "Cup and Balls is a fun component where kinetic energy is generated as you launch the balls.");
addImagesToPrimarySlider("Fun Carousel", imageCollection);
addBigQuestions("Big Qs", listOfBigQuestions);
addVideo("Some cool video", 'http://vimeo.com/97348308/download?t=1402078893&v=259758967&s=fc21a6e94adaecbf8bd1a2579bedb7d4');
addSurvery("Take our awesome survery!");

var tableView = Ti.UI.createTableView({
	// backgroundColor : '#07B5BE',
	backgroundColor : '#FFFFFF',
	data : tableData,
	top : 70
});

$.turnTable.title = "Dolphins are cool";
$.turnTable.add(tableView);
$.turnTable.open();
