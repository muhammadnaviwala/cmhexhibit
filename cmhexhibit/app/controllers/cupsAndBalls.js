var args = arguments[0] || {};
var post_id = args.post_id;

var tableData = [];

function createRow() {
	var row = Ti.UI.createTableViewRow({
		height : 70,
		backgroundColor : '#ffffff'
	});
	return row;
}

function pushRow(row){
	tableData.push(row);
}

var block = Titanium.UI.createView({
	top : 0,
	backgroundColor : '#018F93',
	height : 70,
});

// -----------------------------------------------------------------
var row = createRow();
var eachComponentLabel = Ti.UI.createLabel({
	color : '#474448',
	font : {
		fontFamily : 'Arial',
		fontSize : 20,
		fontWeight : 'bold'
	},
	text : post_id,
	textAlign : 'center',
	height : 50
});
row.add(eachComponentLabel);
pushRow(row);
// -----------------------------------------------------------------


var tableView = Ti.UI.createTableView({
	backgroundColor : '#07B5BE',
	data : tableData,
	top : 73
});
$.cupsAndBalls.add(tableView);
$.cupsAndBalls.open();
